<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

    // Initialize Dropbox client
    $dropbox = new Dropbox\Dropbox(getenv('DBX_TOKEN'));

    // List all the files in a folder
    //$devices = $dropbox->files->list_folder('/C_CLT');

    $n = 20;
    $serialFmt = '/C_CLT/CLT_%1$02d/FFMEN_TECH_%1$02d/ffmen_tech SERIAL_%1$02d.qsn';
    $inputFmt = '/C_CLT/CLT_%1$02d/FFMEN_TECH_%1$02d/ffmen_tech INPUT_%1$02d.qif';
    $devices = array();
    $limitMBs = 10 * 1024 * 1024;
    foreach (range(1, $n) as $i) {

        $device = array();

        $serialContents = $dropbox->files->download(sprintf($serialFmt, $i), false);
        $serial_ini = parse_ini_string($serialContents);
        $device['lastIssuedSerial'] = intval($serial_ini['LastIssuedSerial']);

        $inputContents = $dropbox->files->download(sprintf($inputFmt, $i), false);
        //$input_ini = parse_ini_string($inputContents);
        //$device['inputDataFile'] = $input_ini['InputDataFile'];

        $devices[] = $device;

    }
    
    //dd($devices);

    return view('index', compact('devices'));
});