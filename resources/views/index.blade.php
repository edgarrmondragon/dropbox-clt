<!DOCTYPE html>
<html>
<head>
    <title>Devices</title>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
</head>
<body>

    <h1>Devices</h1>


    <ul class="collection">
    @foreach ($devices as $device)

        {{-- <li class="collection-item"><a href="#">{{ $device['name'] }}</a></li> --}}
        <li class="collection-item"><a href="#">{{ $device['lastIssuedSerial'] }}</a></li>

    @endforeach
    </ul>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>

</body>
</html>